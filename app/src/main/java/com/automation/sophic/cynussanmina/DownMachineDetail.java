package com.automation.sophic.cynussanmina;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.automation.sophic.cynussanmina.Helper.LoadingClass;
import com.automation.sophic.cynussanmina.Model.CoModel;
import com.automation.sophic.cynussanmina.Model.DownTimeModel;
import com.automation.sophic.cynussanmina.Model.MyConfig;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DownMachineDetail extends AppCompatActivity implements View.OnClickListener {

    String PmId="",MachineName="";
    LinearLayout linearMain;
    TextView tvMachineName,tvLineName,tvDateCreated;
    TextView tvStartDate,tvTotalTime,tvTargetDuration;
    TextView tvPmType,tvTittle;
    Button btnBack,btnDone;
    RestCrud RestCrud;
    LoadingClass LoadActivity;
    EditText tvIssue,tvSolution;
    DownTimeModel.GetDownMachineDetail downMachineDetail;
    MyConfig myConfig = MyConfig.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_down_machine_detail);

        LoadActivity = new LoadingClass(this,1);
        PmId=getIntent().getStringExtra("PmId");

        downMachineDetail = new DownTimeModel().new GetDownMachineDetail();

        downMachineDetail= myConfig.getModelDownTime();

        MachineName=getIntent().getStringExtra("MachineName");

        linearMain = (LinearLayout) findViewById(R.id.linear_main);
        tvMachineName = (TextView) findViewById(R.id.tvMachineName);
        tvLineName = (TextView) findViewById(R.id.tvLineName);
        tvDateCreated = (TextView) findViewById(R.id.tvDateCreated);
        tvStartDate = (TextView) findViewById(R.id.tvStartDate);
        tvTotalTime = (TextView) findViewById(R.id.tvTotalTime);
        tvTargetDuration = (TextView) findViewById(R.id.tvTargetDuration);
        tvPmType = (TextView) findViewById(R.id.tvPmType);
        btnBack = (Button) findViewById(R.id.btnBack);
        btnDone = (Button) findViewById(R.id.btnDone);
        tvTittle =  (TextView) findViewById(R.id.tvTitle);
        tvSolution=  (EditText) findViewById(R.id.tvSolution);
        tvIssue =  (EditText) findViewById(R.id.tvIssue);
        tvTittle.setText("DOWNTIME");

        tvIssue.setEnabled(false);
        tvMachineName.setText(MachineName);
        tvTargetDuration.setVisibility(View.GONE);
        RestCrud = APIClient.getClient().create(RestCrud.class);

        //GetCoMachineDetail(PmId);
        AddData();

        btnBack.setOnClickListener(this);
        btnDone.setOnClickListener(this);

    }

    private  void AddData(){

        tvMachineName.setText(downMachineDetail.StationName);
        tvLineName.setText("Line:"+downMachineDetail.LineNameMap);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String dateInString = downMachineDetail.StartDate;
        dateInString=dateInString.replace("T"," ");
        try {
            dateInString = outFormat.format(formatter.parse(dateInString));
        } catch (Exception e) {
            e.printStackTrace();
        }
        tvStartDate.setText("Start Date:"+dateInString);


        Date currentDate = new Date();
        try {
            Date StartDate = formatter.parse(dateInString);
            long diff = currentDate.getTime() - StartDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            tvTotalTime.setText("TotalTime:"+String.valueOf(minutes));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        dateInString = downMachineDetail.DateTime;
        dateInString=dateInString.replace("T"," ");
        try {
            dateInString = outFormat.format(formatter.parse(dateInString));
        } catch (Exception e) {
            e.printStackTrace();
        }
        tvDateCreated.setText("Date Created:"+dateInString);

        tvTargetDuration.setText("TargetDuration:"+String.valueOf(downMachineDetail.Duration));
        tvPmType.setText("Type:"+downMachineDetail.COType);

        if(downMachineDetail.Others.contains(":")){
            String[] Issue = downMachineDetail.Others.split(":");
            tvIssue.setText(Issue[1]);
        }
        else {
            tvIssue.setText(downMachineDetail.Others);
        }

    }

    public void GetCoMachineDetail(String PmId) {

        Call<List<CoModel.GetCoMachineDetail>> call = RestCrud.GetCoMachineDetail(PmId);
        call.enqueue(new Callback<List<CoModel.GetCoMachineDetail>>() {


            @Override
            public void onResponse(Call<List<CoModel.GetCoMachineDetail>> call, Response<List<CoModel.GetCoMachineDetail>> response) {

                List<CoModel.GetCoMachineDetail> body = response.body();
                // if (body == null) {
                //    return;
                // }


                tvMachineName.setText(body.get(0).StationNameMap);
                tvLineName.setText("Line:"+body.get(0).LineNameMap);

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String dateInString = body.get(0).StartDate;
                dateInString=dateInString.replace("T"," ");
                try {
                    dateInString = outFormat.format(formatter.parse(dateInString));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tvStartDate.setText("Start Date:"+dateInString);


                Date currentDate = new Date();
                try {
                    Date StartDate = formatter.parse(dateInString);
                    long diff = currentDate.getTime() - StartDate.getTime();
                    long seconds = diff / 1000;
                    long minutes = seconds / 60;
                    tvTotalTime.setText("TotalTime:"+String.valueOf(minutes));

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                dateInString = body.get(0).DateTime;
                dateInString=dateInString.replace("T"," ");
                try {
                    dateInString = outFormat.format(formatter.parse(dateInString));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tvDateCreated.setText("Date Created:"+dateInString);

                tvTargetDuration.setText("TargetDuration:"+String.valueOf(body.get(0).Duration));
                tvPmType.setText("Type:"+body.get(0).COType);


            }

            @Override
            public void onFailure(Call<List<CoModel.GetCoMachineDetail>> call, Throwable t) {
                Log.d("err","error get GetDownMachineDetail");
            }
        });

    }

    private void DonePM(){

        String body = PmId+","+MainActivity.username;
        Call<String> call = null;

        LoadActivity.show();
        DownTimeModel.ModelAddCloseDowntime modelAddCloseDowntime = new DownTimeModel().new ModelAddCloseDowntime();
        modelAddCloseDowntime.Others = "Solution:"+tvSolution.getText().toString();
        modelAddCloseDowntime.PIC = MainActivity.username;
        modelAddCloseDowntime.Id = downMachineDetail.Id;
        modelAddCloseDowntime.StationName= downMachineDetail.StationName;
        modelAddCloseDowntime.LineNameMap= downMachineDetail.LineNameMap;
        modelAddCloseDowntime.CoType= "DOWNTIME";

        call = RestCrud.CloseDownTime(modelAddCloseDowntime);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                LoadActivity.dismiss();
                String body = response.body();

                if(body.contains("debug"))
                {
                    Toast.makeText(DownMachineDetail.this, "Debug:"+body, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent();
                    //intent.putExtra("pos", 1);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
                else if(body.contains("true")){
                    Toast.makeText(DownMachineDetail.this, "Downtime Closed!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent();
                    //intent.putExtra("pos", 1);
                    setResult(Activity.RESULT_OK, intent);

                    finish();
                }
                else{
                    Toast.makeText(DownMachineDetail.this, "Error: "+body, Toast.LENGTH_SHORT).show();
                }



            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                LoadActivity.dismiss();
                Toast.makeText(DownMachineDetail.this, "Error Updating Data !", Toast.LENGTH_SHORT).show();
                call.cancel();

                Intent intent = new Intent();
                //intent.putExtra("pos", 1);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

    }

    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnBack:
                Intent intent = new Intent();
                //intent.putExtra("pos", 1);
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
                break;

            case R.id.btnDone:
                String solution = tvSolution.getText().toString();
                if(solution.isEmpty()){
                    Toast.makeText(DownMachineDetail.this,"Please Key In Solution",Toast.LENGTH_LONG).show();
                    return;
                }

                DonePM();
                break;

            default:

                break;
        }
    }

}
