package com.automation.sophic.cynussanmina.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by X13 on 1/4/2017.
 */

public class JsonModel {
    public String Id;
    public String MachineName;
    public String LineName;
    public String ModuleNo;
    public String ErrorCode;
    public String cause;
    public String remedy;
    public String PICName;
    public String onoff;
    public String DataLineName;
    public String DataMachineName;
    public String MachineType;
    public String WaitingTime;
    public String TimeIn;
    public  String productName;
    @SerializedName("status")
    public String Status;
}
