package com.automation.sophic.cynussanmina.Helper;

import android.app.AlertDialog;
import android.content.Context;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import com.automation.sophic.cynussanmina.R;

import dmax.dialog.SpotsDialog;
/**
 * Created by X-PC on 2/4/2018.
 */

public class LoadingClass {

    // ProgressDialog progressDialog;
    AlertDialog progressDialog;
    Context context;

    public LoadingClass(Context context, int type,String... CustomMessage){

        this.context = context;

        switch (type)
        {
            case 1:
                progressDialog = new SpotsDialog(context,"Please Wait Loading", R.style.Custom);
                break;

            case 2:
                progressDialog = new SpotsDialog(context,"Authenticating",R.style.Custom);
                break;

            case 3:
                progressDialog = new SpotsDialog(context,CustomMessage[0],R.style.Custom);
                break;
        }

    }

    public void show(){
        //progressDialog.setIndeterminate(true);
        //progressDialog.setMessage("Loading");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(false);
    }

    public void dismiss(){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }
}
