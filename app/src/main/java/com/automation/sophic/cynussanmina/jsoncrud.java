package com.automation.sophic.cynussanmina;

import android.util.Log;

import com.automation.sophic.cynussanmina.Model.JsonModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by X13 on 1/4/2017.
 */

public class jsoncrud {

    public ArrayList<String> AlertList = new ArrayList<String>();
    JSONObject jsonObj;

    public ArrayList<String> getAlertList() {
        return AlertList;
    }

    public void read(String jsonStr,String getackdata){



        Date dt2 = null;
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String date = dateFormat.format(Calendar.getInstance().getTime());


        Gson gson = new Gson();
        Type type = new TypeToken<List<JsonModel>>(){}.getType();
        List<JsonModel> modellist = gson.fromJson(jsonStr, type);
        for (JsonModel model : modellist){
        String s = model.Id+"_"+model.MachineName+"_"+model.LineName+"_"+model.ModuleNo+"_"+model.ErrorCode+"_"+model.cause+"_"+model.remedy;

            if(model.ErrorCode.contains(",")){

                String[] rowData = model.ErrorCode.split(Pattern.quote(","));
                Log.d("xxx", "HERE");

                String Cpk6 = rowData[0];
                String Cpk4 = rowData[1];
                String Cpk2 = rowData[2];
                String CpkC = rowData[3];
                String Trend = rowData[4];
                AlertList.add(model.Id+"~"+model.DataLineName+"~"+model.productName+"~"+model.ModuleNo+"~"
                +Cpk6+"~"
                +Cpk4+"~"
                +Cpk2+"~"
                +CpkC+"~"
                +Trend+"~"
                +model.cause+"~"+model.remedy+"~"+model.PICName+"~"+model.TimeIn+"~"+model.WaitingTime);

            }
            else{
                Log.d("xxx", "HERE2");
                AlertList.add(model.Id+"~"+model.DataLineName+"~"+model.DataMachineName+"~"+model.ModuleNo+"~"+model.ErrorCode+"~"+model.cause+"~"+model.remedy+"~"+model.PICName+"~"+model.TimeIn+"~"+model.WaitingTime);

            }

        }

    }


}


