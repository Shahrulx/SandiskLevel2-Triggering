package com.automation.sophic.cynussanmina;

/**
 * Created by andrei on 2/27/17.
 */

public interface NetworkCallback {
    void requestDone(String result);
}
