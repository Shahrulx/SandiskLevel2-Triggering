package com.automation.sophic.cynussanmina;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import com.automation.sophic.cynussanmina.Model.MyConfig;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class table_display extends AppCompatActivity {
    Timer myTimer = new Timer();
    private Handler sleepHandler = new Handler();
    private Runnable sleep;
    private final int SLEEPTIME = 5000; // 10seconds
    private boolean sleepLCD = false;
    public  TextView txt_last,txt_ListHeader;
    public static TextView txt_TotalData,txt_UserName;
    private Button btn_NewAlert,btn_AlertHistory,btnBack;
    private NetworkCallback netCallback;
    private NetworkTask netTask;
    private Window mWindow;
    private ListView mList;
    private DataAdapter adapter;
    private String[] tableData;
    private String memData;
    private jsoncrud jsoncrud;
    private int PrevDataCount = 0;
    private int CurrentDataCount = 0;
    private Vibrator vibrator;
    public  String MachineType;
    private static final String[] EMPTY_STRING_ARRAY = new String[0];
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    Intent intent;
    private GoogleApiClient client;
    private String getnewalert = "true";
    String curdata= "";
    String prevdata="";
    Boolean NotificationStat = true;
    MyConfig myConfig = MyConfig.getInstance();
    String LineId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_display);
        vibrator = (Vibrator)this.getSystemService(Context.VIBRATOR_SERVICE);
        jsoncrud = new jsoncrud();
        //Don't let device go in suspend
        memData = "";
        mWindow = this.getWindow();
        mWindow.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        txt_last = (TextView) findViewById(R.id.txt_last);
        txt_TotalData = (TextView) findViewById(R.id.txt_TotalData);
        txt_ListHeader = (TextView) findViewById(R.id.txt_ListHeader);
        txt_UserName = (TextView) findViewById(R.id.txt_UserName);
        btn_NewAlert =(Button) findViewById(R.id.btn_NewPM);
        btn_AlertHistory=(Button) findViewById(R.id.btn_AlertHistory);
        btnBack=(Button) findViewById(R.id.btnBack);

        mList = (ListView) findViewById(R.id.data_list);

        Intent intent = getIntent();
        MachineType = intent.getStringExtra("MachineType");
        LineId=intent.getStringExtra("LineId");

        Log.d("PPhere", MachineType + LineId);

        intent = new Intent(this, table_display.class);

        netCallback = new NetworkCallback() {
            @Override
            public void requestDone(final String result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (result.equals("-1"))
                            return;
                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss yyyy-MM-dd");
                        String currentDateandTime = sdf.format(new Date());
                        txt_last.setText("Last Update :" + currentDateandTime);
                       // txt_UserName.setText(" Username: " + MainActivity.username);

                        //txt_last.setText(MainActivity.username);

                        curdata=result;
                        Log.d("logsx",result);
                        if(prevdata.equals("")){
                            prevdata=result;
                            FillUpInformation(result);
                        }
                        else{
                            if(curdata.equals(prevdata)){

                            }
                            else {
                                if(getnewalert.contains("true")){
                                    showNotification();

                                }
                                FillUpInformation(result);
                                prevdata=result;
                            }
                        }
                    }
                });
            }
        };
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Initialization();
            }
        }, 500,2000); // initial delay 1 second, interval 1 second



        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        btn_NewAlert.setBackgroundResource(R.drawable.buttonselected);
        btn_AlertHistory.setBackgroundResource(R.drawable.buttonunselected2);

        btn_AlertHistory.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Toast.makeText(table_display.this, "Please wait loading data....", Toast.LENGTH_LONG).show();
                adapter = null;
                mList.setAdapter(adapter);
                getnewalert = "false";
                Initialization();
                btn_AlertHistory.setBackgroundResource(R.drawable.buttonselected);
                btn_NewAlert.setBackgroundResource(R.drawable.buttonunselected2);
                txt_ListHeader.setText("Assigned");

               // FillUpInformation("");

            }
        });

        btn_NewAlert.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Toast.makeText(table_display.this, "Please wait loading data....", Toast.LENGTH_LONG).show();
                adapter = null;
                mList.setAdapter(adapter);
                prevdata = "";
                getnewalert = "true";
                Initialization();
                btn_NewAlert.setBackgroundResource(R.drawable.buttonselected);
                btn_AlertHistory.setBackgroundResource(R.drawable.buttonunselected2);
                txt_ListHeader.setText("New Alert");

                //FillUpInformation("");
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        myConfig.setTableDisplayIsVisible(true);
    }


    @Override
    public void onUserInteraction() {
        if (sleepLCD) {
            WindowManager.LayoutParams params = mWindow.getAttributes();
            params.screenBrightness = 1;
            mWindow.setAttributes(params);
            sleepLCD = false;
        }
        sleepHandler.removeCallbacks(sleep);
        sleepHandler.postDelayed(sleep, SLEEPTIME);
    }

    @Override
    protected void onPause() {
        //If you pause the app we need to cancel the timer otherwise it server communication
        //will continue in the background
        //here
        //myTimer.cancel();
        super.onPause();
    }

    @Override
    protected void onResume() {
        //On resume init the timer again.

/*
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Initialization();
            }
        }, 500, 60000); // initial delay 1 second, interval 1 second
*/
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        //If the app is killed make sure to clear stuff
        myTimer.cancel();
        myTimer = null;
        adapter = null;
        memData = null;
        tableData = null;
        netTask = null;
        myConfig.setTableDisplayIsVisible(false);
        super.onDestroy();
    }

    public void Initialization() {
        String URL = APIClient.getURL();
        if(getnewalert.equals("true"))
        {
            URL = URL+ "RestApi/GetAlert/"+MachineType+","+LineId;
        }

        else
            URL = URL+"RestApi/GetAlertByPic/"+MachineType+","+MainActivity.username+","+LineId;

        Log.d("URL",URL);
        netTask = new NetworkTask();
        netTask.setCallback(netCallback);
        netTask.execute(URL);
    }
    public void FillUpInformation(String data) {

        jsoncrud.AlertList.clear();
        Log.d("datas",data);
       // jsoncrud.read("[{\"alert_id\":\"14\",\"detail_message\":\"<html tag>\",\"short_message\":\"8 false calls\",\"alerted_datetime\":\"2017/04/02 22:19:10\",\"acknowledge_datetime\":\"\",\"origin\":\"v510 - bebaru line\",\"user_acknowledged\":\"false\",\"authorised_users\":\"new_user\",\"authorised_groups\":\"managers\",\"acknowledged_by\":\"\",\"extra_message\":\"extra long message\"},{\"alert_id\":\"15\",\"detail_message\":\"<html tag>\",\"short_message\":\"8 false calls\",\"alerted_datetime\":\"2017/04/02 22:19:26\",\"acknowledge_datetime\":\"\",\"origin\":\"v510 - bebaru line\",\"user_acknowledged\":\"false\",\"authorised_users\":\"new_user\",\"authorised_groups\":\"managers\",\"acknowledged_by\":\"\",\"extra_message\":\"extra long message\"},{\"alert_id\":\"16\",\"detail_message\":\"<html tag>\",\"short_message\":\"8 false calls\",\"alerted_datetime\":\"2017/04/02 22:19:27\",\"acknowledge_datetime\":\"\",\"origin\":\"v510 - bebaru line\",\"user_acknowledged\":\"false\",\"authorised_users\":\"new_user\",\"authorised_groups\":\"managers\",\"acknowledged_by\":\"\",\"extra_message\":\"extra long message\"}]",getnewalert);
        if(data.isEmpty()){
            return;
        }
        else if(data == null){
            return;
        }
        else if (data=="[]") {
            return;
        }

        jsoncrud.read(data,getnewalert);


        MyCustomAdapter adapter = new MyCustomAdapter(jsoncrud.AlertList,getnewalert, this);
        mList.setAdapter(adapter);

        CurrentDataCount = jsoncrud.AlertList.size();

        if(PrevDataCount ==0){
            Log.d("MNM","oldata");
            PrevDataCount = CurrentDataCount;
        }
        if(PrevDataCount != CurrentDataCount){
            PrevDataCount = CurrentDataCount;
            Log.d("MNM","newData");
            vibrator.vibrate(800);
        }

        txt_TotalData.setText("Total:"+jsoncrud.AlertList.size());
    }


    private TextView NewTextView(String content) {
        TextView txtView = new TextView(table_display.this);
        txtView.setPadding(0, 0, 0, 0);
        txtView.setText(content);
        txtView.setTextColor(Color.BLACK);
        txtView.setTextSize(15);
        txtView.setGravity(Gravity.CENTER);
        return txtView;
    }

    private LinearLayout NewCell() {
        TableRow.LayoutParams llp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        LinearLayout cell = new LinearLayout(this);
        cell.setBackgroundColor(Color.TRANSPARENT);
        cell.setBackgroundResource(R.drawable.cell_header);
        cell.setLayoutParams(llp);//2px border on the right for the cell
        cell.setGravity(Gravity.CENTER);
        return cell;

    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("table_display Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    public static void UpdateTotal(int Count){
        txt_TotalData.setText("Total:"+Count);
    }

    public static int Total(){
        String ii = txt_TotalData.getText().toString();
        ii = ii.replace("Total:", "");

        int c = Integer.parseInt(ii);

        return  c;
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    public void showNotification() {
        //PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
       /*
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        Resources r = getResources();
        Notification notification = new NotificationCompat.Builder(this)
                .setTicker("x")
                .setSmallIcon(android.R.drawable.ic_menu_report_image)
                .setContentTitle("C")
                .setContentText("V")
                .setDefaults(Notification.DEFAULT_SOUND)
                .setContentIntent(pi)
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
        */

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        r.play();
    }

    public class SetText {

    }
}

