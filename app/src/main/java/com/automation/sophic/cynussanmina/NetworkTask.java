package com.automation.sophic.cynussanmina;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;

public class NetworkTask extends AsyncTask<String, Void, String> {
    private final String TAG = "Cygnus.Sanmina";
    private int readTimeout = 10000; /* milliseconds */
    private int connectTimeout = 15000; /* milliseconds */
    private NetworkCallback mCallback;

    @Override
    protected String doInBackground(String... strings) {
        try {
            return makeRequest(strings[0]);
        } catch (IOException e) {
            return "-1";
        }
    }

    @Override
    protected void onPostExecute(String result) {
        //Here you are done with the task
        if(mCallback != null){
            mCallback.requestDone(result);
        }
    }

    public void setCallback(NetworkCallback callback){
        mCallback = callback;
    }

    public void setReadTimeout(int timeout){
        readTimeout = timeout;
    }

    public void setConnectTimeout(int timeout){
        connectTimeout = timeout;
    }

    private String makeRequest(String myurl) throws IOException {
        String contentAsString = "0";
        InputStream is = null;
        int length = 500;
        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(readTimeout);
            conn.setConnectTimeout(connectTimeout);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "Server replied with code: " + response);
            is = conn.getInputStream();
            // Convert the InputStream into a string
            contentAsString = convertInputStreamToString(is);
            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            Log.d(TAG, "["+currentDateTimeString+"] Server answered: "+contentAsString);
            return contentAsString;
        }
        finally {
            if (is != null) {
                is.close();
            }
        }



    }


    public String convertInputStreamToString(InputStream stream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append(' ');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}