package com.automation.sophic.cynussanmina;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.automation.sophic.cynussanmina.Helper.AlertHelper;
import com.automation.sophic.cynussanmina.Model.CoModel;
import com.automation.sophic.cynussanmina.Model.JsonModel3;
import com.automation.sophic.cynussanmina.Model.Model_AllMachine;
import com.automation.sophic.cynussanmina.Model.MyConfig;
import com.automation.sophic.cynussanmina.Model.PmModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EventListener;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListAllMachine extends AppCompatActivity implements  android.view.View.OnClickListener  {


    RestCrud RestCrud;
    TextView txtMachineName,tvWifiSSD;
    RadioGroup rgroupCheckList;
    Button line1,line2,line3,btnLogout,btnOthers;
    TextView displayuserid,tvLastUpdate;
    private TextView battery,wifi;
    AlertHelper alertHelper;
    Context mycontext;
    MyConfig myConfig = MyConfig.getInstance();
    int PmCureentData=0,PmPrevData=0;
    int CoCureentData=0,CoPrevData=0;
    boolean InitCo=true;
    boolean InitPm=true;
    boolean FirstRun=true;


    private BroadcastReceiver batteryinfo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL,0);
            battery.setText(String.valueOf(level)+"%");

            if (level>=0 && level<=25)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery1, 0, 0, 0);
            else if(level>=26 && level<=50)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery2, 0, 0, 0);
            else if(level>=51 && level<=75)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery3, 0, 0, 0);
            else if(level>=71 && level<=100)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery4, 0, 0, 0);
        }
    };

    @Override
    public void onResume()
    {
        super.onResume();
        wifi();

    }

    public void onDestroy() {

        Log.d("call","call");
        MyConfig.TaskListPM.removeCallbacks(UpdatePMCO);
        MyConfig.TaskListMachine.removeCallbacks(runnableCode);
        unregisterReceiver(batteryinfo);
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_allmachine);

        displayuserid = (TextView) findViewById(R.id.displayuserid);
        tvLastUpdate = (TextView) findViewById(R.id.tvLastUpdate);
        btnLogout = (Button) findViewById(R.id.btnLogout);
        line1 = (Button) findViewById(R.id.line1);
        line2 = (Button) findViewById(R.id.line2);
        line3 = (Button) findViewById(R.id.line3);
        btnOthers = (Button) findViewById(R.id.btnOthers);

        battery = (TextView) findViewById(R.id.battery);
        tvWifiSSD = (TextView) findViewById(R.id.tvWifiSSD);
        wifi = (TextView) findViewById(R.id.wifi);
        this.registerReceiver(this.batteryinfo,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));


        RestCrud = APIClient.getClient().create(RestCrud.class);
        alertHelper = new AlertHelper();
        mycontext = getApplicationContext();

        displayuserid.setText("USERNAME:"+MainActivity.username);
        Initialization();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss yyyy-MM-dd");
        String currentDateandTime = sdf.format(new Date());
        tvLastUpdate.setText("Last Update :" + currentDateandTime);

        if(savedInstanceState==null){
            MyConfig.TaskListMachine = new Handler();
            MyConfig.TaskListMachine.post(runnableCode);
            MyConfig.TaskListPM = new Handler();
            MyConfig.TaskListPM.post(UpdatePMCO);
        }



        //set button click
        line1.setOnClickListener(this);
        line2.setOnClickListener(this);
        line3.setOnClickListener(this);
        btnOthers.setOnClickListener(this);

        btnLogout.setOnClickListener(this);

        GetMachineList();
    }

    private void GetMachineList()
    {
        RestCrud = APIClient.getClient().create(RestCrud.class);
        Call<List<JsonModel3.AllMachineList>> call = null;
        call = RestCrud.GetMachineList();

        call.enqueue(new Callback<List<JsonModel3.AllMachineList>>() {

            @Override
            public void onResponse(Call<List<JsonModel3.AllMachineList>> call, Response<List<JsonModel3.AllMachineList>> response) {

                List<JsonModel3.AllMachineList> allMachineLists = response.body();
            }

            @Override
            public void onFailure(Call<List<JsonModel3.AllMachineList>> call, Throwable t) {

            }
        });
    }

    public void wifi(){

        WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        int numberOfLevels = 5;
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int level = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
        int rssi = wifiInfo.getRssi();
        tvWifiSSD.setText(wifiInfo.getSSID());
        int percentage = (int) ((level/numberOfLevels)*100);

        if (rssi>=-20 && rssi<=-1)
            wifi.setText("100%");
        else if(rssi<=-51 && rssi>=-55)
            wifi.setText("90%");
        else if(rssi<=-56 && rssi>=-62)
            wifi.setText("80%");
        else if(rssi<=-63 && rssi>=-65)
            wifi.setText("75%");
        else if(rssi<=-66 && rssi>=-68)
            wifi.setText("70%");
        else if(rssi<=-69 && rssi>=-60)
            wifi.setText("60%");
        else if(rssi<=-75 && rssi>=-79)
            wifi.setText("50%");
        else if(rssi<=-80 && rssi>=-83)
            wifi.setText("30%");

        Log.d("wifi", "wifi: "+level);
        Log.d("wifi", "wifi: "+wifiInfo.getRssi());

        switch (level)
        {
            case 0: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_0_bar_teal_a700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_0_bar_teal_a700_24dp, 0, 0, 0);

                break;

            case 1: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_1_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_1_bar_teal_700_24dp, 0, 0, 0);

                break;

            case 2: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_2_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_2_bar_teal_700_24dp, 0, 0, 0);

                break;

            case 3: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_3_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_3_bar_teal_700_24dp, 0, 0, 0);

                break;

            case 4: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_4_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_4_bar_teal_700_24dp, 0, 0, 0);

                break;
        }


    }


    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            Initialization();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //if(myConfig.isTaskListAllMachine()==true){
                    myConfig.setTaskListAllMachine(true);

                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss yyyy-MM-dd");
                    String currentDateandTime = sdf.format(new Date());
                    tvLastUpdate.setText("Last Update :" + currentDateandTime);

                    // }
                    Log.d("ISRUN","LSITRUNING");
                }
            });
            MyConfig.TaskListMachine.postDelayed(runnableCode, 2000);
        }
    };

    private Runnable UpdatePMCO = new Runnable() {
        @Override
        public void run() {
            UpdatePM();
            UpdateCO();
            wifi();
            MyConfig.TaskListPM.postDelayed(UpdatePMCO, 3000);
        }
    };




    public void ShowDialog(String Message){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(Message);
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "OK",

                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ActivityManager am = (ActivityManager)mycontext.getSystemService(Context.ACTIVITY_SERVICE);
                        dialog.cancel();

                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
        Button button = alert11.getButton(AlertDialog.BUTTON_POSITIVE);
        button.setHeight(20);
    }

    public void Initialization() {

        Call<List<Model_AllMachine>> call = RestCrud.CountMachineAlert();
        call.enqueue(new Callback<List<Model_AllMachine>>() {


            @Override
            public void onResponse(Call<List<Model_AllMachine>> call, Response<List<Model_AllMachine>> response) {

                List<Model_AllMachine> body = response.body();
                if (body == null) {
                   return;
                }

                Log.d("Res",response.body().toString());

                List<Model_AllMachine> resource = response.body();


                if(resource == null)
                    return;

                int numofdata = resource.size();

                if (numofdata > 0) {

                    for (int i = 0; i < resource.size(); i++) {
                        String MachineType= resource.get(i).MachineType;
                        String ErrorCount= resource.get(i).ErrorCount;
                        String OtherParam= resource.get(i).OtherParam;

                        switch (MachineType){

                            case "NXT":
                                myConfig.setNxtCurrent(Integer.valueOf(ErrorCount));

                                Log.d("ErrorCountX", "onResponse: "+ErrorCount);

                                if (myConfig.getNxtPrevious() == 0) {
                                    myConfig.setNxtPrevious(myConfig.getNxtCurrent());
                                }
                                else{
                                    UpdateErrors(line1, myConfig.getNxtCurrent(), myConfig.getNxtPrevious(), "NXT");

                                }

                                if (myConfig.getNxtCurrent() != myConfig.getNxtPrevious()) {
                                    myConfig.setNxtPrevious(myConfig.getNxtCurrent());
                                } else if (myConfig.getNxtCurrent() > 0) {
                                    myConfig.setNxtPrevious(myConfig.getNxtCurrent());
                                }


                                break;

                            case "SPI":
                                myConfig.setSPICurrent(Integer.valueOf(ErrorCount));

                                if (myConfig.getSPIPrevious() == 0) {
                                    myConfig.setSPIPrevious(myConfig.getSPICurrent());
                                }
                                else{
                                    UpdateErrors(line2, myConfig.getSPICurrent(), myConfig.getSPIPrevious(), "SPI");
                                }

                                if (myConfig.getSPICurrent() != myConfig.getSPIPrevious()) {
                                    myConfig.setSPIPrevious(myConfig.getSPICurrent());
                                } else if (myConfig.getSPICurrent() > 0) {
                                    myConfig.setSPIPrevious(myConfig.getSPICurrent());
                                }
                                break;

                            case "DEK":
                                myConfig.setDEKCurrent(Integer.valueOf(ErrorCount));
                                Log.d("dekcount",ErrorCount);
                                if (myConfig.getDEKPrevious() == 0) {
                                    myConfig.setDEKPrevious(myConfig.getDEKCurrent());
                                }
                                else{
                                    UpdateErrors(line3, myConfig.getDEKCurrent(), myConfig.getDEKPrevious(), "DEK");

                                }

                                if (myConfig.getDEKCurrent() != myConfig.getDEKPrevious()) {
                                    myConfig.setDEKPrevious(myConfig.getDEKCurrent());
                                } else if (myConfig.getDEKCurrent() > 0) {
                                    myConfig.setDEKPrevious(myConfig.getDEKCurrent());
                                }
                                break;
                        }

                    }

                }

                if(FirstRun){
                    FirstRun=false;
                }
                else{
                    myConfig.setTaskListAllMachine(false);
                }

            }

            @Override
            public void onFailure(Call<List<Model_AllMachine>> call, Throwable t) {
                Log.d("err","error get");
            }
        });

    }

    public void UpdatePM() {

        Call<List<PmModel.GetPmLine>> call = RestCrud.GetPmLine();
        call.enqueue(new Callback<List<PmModel.GetPmLine>>() {


            @Override
            public void onResponse(Call<List<PmModel.GetPmLine>> call, Response<List<PmModel.GetPmLine>> response) {

                List<PmModel.GetPmLine> body = response.body();
                if (body == null) {
                    return;
                }

                Log.d("Res",response.body().toString());

                List<PmModel.GetPmLine> resource = response.body();


                if(resource == null)
                    return;

                int numofdata = resource.size();
                PmCureentData = numofdata;

                if(InitPm)
                {
                    PmPrevData=PmCureentData;

                }

                else if(PmPrevData==PmCureentData){
                    return;
                }

                else
                {
                    PmPrevData=PmCureentData;
                }


                Log.d("pm updated","Data update"+numofdata);
                int i = 0;

                if (numofdata > 0) {

                    if(InitPm==true){
                        InitPm=false;
                    }

                    else{

                    }
                    alertHelper.showNotification(mycontext);
                    alertHelper.StartVibrate(mycontext);
                   // ShowDialog("PM UPDATED PRESS OK TO CLOSE");

                }
            }

            @Override
            public void onFailure(Call<List<PmModel.GetPmLine>> call, Throwable t) {
                Log.d("err","error get");
            }
        });
    }

    public void UpdateCO() {

        Call<List<CoModel.GetCoLine>> call = RestCrud.GetCoLine();
        call.enqueue(new Callback<List<CoModel.GetCoLine>>() {


            @Override
            public void onResponse(Call<List<CoModel.GetCoLine>> call, Response<List<CoModel.GetCoLine>> response) {

                List<CoModel.GetCoLine> body = response.body();
                if (body == null) {
                    return;
                }

                Log.d("Res",response.body().toString());

                List<CoModel.GetCoLine> resource = response.body();


                if(resource == null)
                    return;

                int numofdata = resource.size();
                CoCureentData = numofdata;

                if(InitCo)
                {
                    CoPrevData=CoCureentData;

                }

                else if(CoPrevData==CoCureentData){
                    return;
                }

                else
                {
                    CoPrevData=CoCureentData;
                }


                Log.d("co updated","Data update co"+numofdata+InitCo);
                int i = 0;

                if (numofdata > 0) {

                    if(InitCo){
                        InitCo=false;
                    }
                    else
                    {

                    }

                    alertHelper.showNotification(mycontext);
                    alertHelper.StartVibrate(mycontext);
                    //ShowDialog("CO UPDATED PRESS OK TO CLOSE");
                }
            }

            @Override
            public void onFailure(Call<List<CoModel.GetCoLine>> call, Throwable t) {
                Log.d("err","error get");
            }
        });
    }


    @SuppressLint("WrongConstant")
    private  void UpdateErrors(Button mybtn, int currentval, int preval, String MachineName){

        mybtn.setText(MachineName +"\n"+ currentval);

        if(currentval != preval){

            /*
            ObjectAnimator anim = ObjectAnimator.ofInt(mybtn,"backgroundColor",Color.RED,Color.parseColor("#a30000"),Color.RED);
            anim.setDuration(1000);
            anim.setEvaluator(new ArgbEvaluator());
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            anim.start();
            */

            mybtn.setBackgroundResource(android.R.color.holo_red_dark);

            if(currentval > preval){
                alertHelper.showNotification(mycontext);
                alertHelper.StartVibrate(mycontext);

               //ShowDialog("NEW ALERT IN "+MachineName+".PRESS OK TO CLOSE");

                if(myConfig.isTableDisplayIsVisible())
                    table_display.txt_UserName.setText("FOUND NEW ALERT");


                Toast.makeText(ListAllMachine.this,"NEW ALERT IN "+MachineName, Toast.LENGTH_LONG).show();

            }
        }

        else if(currentval  > 0){
            /*
            ObjectAnimator anim = ObjectAnimator.ofInt(mybtn,"backgroundColor",Color.RED,Color.parseColor("#a30000"),Color.RED);
            anim.setDuration(1000);
            anim.setEvaluator(new ArgbEvaluator());
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            anim.start();
            */
            mybtn.setBackgroundResource(android.R.color.holo_red_dark);
        }

        else{
            mybtn.setBackgroundResource(android.R.color.holo_green_dark);
        }

    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.line1:
                //intent = new Intent(ListAllMachine.this, table_display.class);
                //
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("Type", "MachinePage");
                intent.putExtra("MachineType", "NXT");
                startActivity(intent);
                break;
            case R.id.line2:
                //intent = new Intent(ListAllMachine.this, table_display.class);
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "SPI");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case R.id.line3:
                //intent = new Intent(ListAllMachine.this, table_display.class);
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "DEK");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case R.id.btnOthers:
                intent = new Intent(ListAllMachine.this, OtherOptions.class);
                startActivity(intent);
                break;
            case R.id.btnPM:
                //myConfig.myTimer.cancel();
                intent = new Intent(ListAllMachine.this, PMMainPage.class);
                intent.putExtra("Type","LinePage");
                myConfig.setCurrentPage("PmLine");
                startActivity(intent);
                break;

            case R.id.btnCO:
                //myConfig.myTimer.cancel();
                intent = new Intent(ListAllMachine.this, COMainPage.class);
                intent.putExtra("Type","LinePage");
                myConfig.setCoCurrentPage("CoLine");
                startActivity(intent);
                break;

            case R.id.btnLogout:
                //myConfig.myTimer.cancel();
                finish();
                break;
            default:
                break;
        }
    }


}
