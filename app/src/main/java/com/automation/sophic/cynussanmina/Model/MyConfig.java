package com.automation.sophic.cynussanmina.Model;

import android.os.Handler;

import java.util.Timer;


/**
 * Created by X-PC on 19/2/2018.
 */

public class MyConfig {

    private static MyConfig instance = null;
    protected MyConfig() {
        // Exists only to defeat instantiation.
    }
    public static MyConfig getInstance() {
        if(instance == null) {
            instance = new MyConfig();
        }
        return instance;
    }

    public static boolean isTaskListPM=false;
    public static Handler TaskListPM;

    public static boolean isTaskListCO=false;
    public static Handler TaskListCO;

    public static boolean isTaskMachineList=false;
    public static Handler TaskListMachine;
    //public Timer myTimer;
    //public Timer PmListTimer;
    //public Timer CoListTimer;
    //public Timer PmMachineTimer;


    private String CurrentPage="";

    public boolean isTableDisplayIsVisible() {
        return TableDisplayIsVisible;
    }

    public void setTableDisplayIsVisible(boolean tableDisplayIsVisible) {
        TableDisplayIsVisible = tableDisplayIsVisible;
    }

    private boolean TableDisplayIsVisible=false;


    public String getCurrentPage() {
        return CurrentPage;
    }

    public void setCurrentPage(String _CurrentPage) {
        CurrentPage = _CurrentPage;
    }


    private String CoCurrentPage="";

    public String getCoCurrentPage() {
        return CoCurrentPage;
    }
    public void setCoCurrentPage(String _CoCurrentPage) {
        CoCurrentPage = _CoCurrentPage;
    }



    //region ListAllMachine Variable
    private boolean TaskListAllMachine=true;

    public boolean isTaskListAllMachine() {
        return TaskListAllMachine;
    }

    public void setTaskListAllMachine(boolean taskListAllMachine) {
        TaskListAllMachine = taskListAllMachine;
    }

    private boolean TaskGetLineByMachine=true;

    public boolean isTaskGetLineByMachine() {
        return TaskGetLineByMachine;
    }

    public void setTaskGetLineByMachine(boolean _TaskGetLineByMachine) {
        TaskGetLineByMachine = _TaskGetLineByMachine;
    }

    private boolean TaskListPMCO=true;

    public boolean isTaskListPMCO() {
        return TaskListPMCO;
    }

    public void setTaskListPMCO(boolean taskListPMCO) {
        TaskListPMCO = taskListPMCO;
    }






    private int NxtPrevious=0;
    private int NxtCurrent;

    public int getNxtPrevious() {
        return NxtPrevious;
    }

    public void setNxtPrevious(int nxtPrevious) {
        NxtPrevious = nxtPrevious;
    }

    public int getNxtCurrent() {
        return NxtCurrent;
    }

    public void setNxtCurrent(int nxtCurrent) {
        NxtCurrent = nxtCurrent;
    }


    private int SPIPrevious=0;
    private int SPICurrent;

    public int getSPIPrevious() {
        return SPIPrevious;
    }

    public void setSPIPrevious(int SPIPrevious) {
        this.SPIPrevious = SPIPrevious;
    }

    public int getSPICurrent() {
        return SPICurrent;
    }

    public void setSPICurrent(int SPICurrent) {
        this.SPICurrent = SPICurrent;
    }

    private int DEKPrevious=0;
    private int DEKCurrent;

    public int getDEKPrevious() {
        return DEKPrevious;
    }

    public void setDEKPrevious(int DEKPrevious) {
        this.DEKPrevious = DEKPrevious;
    }

    public int getDEKCurrent() {
        return DEKCurrent;
    }

    public void setDEKCurrent(int DEKCurrent) {
        this.DEKCurrent = DEKCurrent;
    }


    private DownTimeModel.GetDownMachineDetail modelDownTime;

    public void setModelDownTime(DownTimeModel.GetDownMachineDetail modelDownTime) {
        this.modelDownTime = modelDownTime;
    }
    public DownTimeModel.GetDownMachineDetail getModelDownTime() {
        return modelDownTime;
    }


    //endregion
}
