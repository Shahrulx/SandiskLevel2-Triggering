package com.automation.sophic.cynussanmina;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.automation.sophic.cynussanmina.Helper.AlertHelper;
import com.automation.sophic.cynussanmina.Helper.LoadingClass;
import com.automation.sophic.cynussanmina.Model.CoModel;
import com.automation.sophic.cynussanmina.Model.DownTimeModel;
import com.automation.sophic.cynussanmina.Model.JsonModel3;
import com.automation.sophic.cynussanmina.Model.MyConfig;
import com.automation.sophic.cynussanmina.Model.PmModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPMActivity extends AppCompatActivity implements
        android.view.View.OnClickListener {

    private static final String TAG = "NewPm";

    private TextView mDisplayDate;
    private TextView mDisplayTime;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    Spinner spinnerLine,spinnerMachine;
    ArrayList<String> LineArray = new ArrayList<String>();
    ArrayList<String> MachineArray = new ArrayList<String>();
    Button btnSavePm,btnBack;
    RestCrud RestCrud;
    TextView tvPMType,tvPmDuration,tvPMDate,tvPmTime,tvTittle,tvTypeText,lblDuration;
    String NewData="";
    AlertHelper alertHelper;
    MyConfig myConfig = MyConfig.getInstance();
    String PageType="";
    LoadingClass LoadActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_newpm);

        alertHelper = new AlertHelper();
        LoadActivity = new LoadingClass(this,1);

        mDisplayDate = (TextView) findViewById(R.id.tvPMDate);
        mDisplayTime = (TextView) findViewById(R.id.tvPmTime);
        RestCrud = APIClient.getClient().create(RestCrud.class);
        spinnerLine=(Spinner)findViewById(R.id.spinnerLine);
        spinnerMachine=(Spinner)findViewById(R.id.spinnerMachine);
        btnSavePm =(Button)findViewById(R.id.btnSavePm);
        btnBack =(Button)findViewById(R.id.btnBack);
        tvTittle = (TextView) findViewById(R.id.tvStartDate);
        tvTypeText = (TextView) findViewById(R.id.tvTypeText);
        tvPMType= (TextView) findViewById(R.id.tvPMType);
        tvPmDuration = (TextView) findViewById(R.id.tvPmDuration);
        tvPMDate = (TextView) findViewById(R.id.tvPMDate);
        tvPmTime = (TextView) findViewById(R.id.tvPmTime);
        lblDuration = (TextView) findViewById(R.id.textView22);

        PageType=getIntent().getStringExtra("Type");

        if(PageType.equals("PM")){
            tvTittle.setText("New Prevention Maintenance");
            tvTypeText.setText("Preventive Maintenance Type");
        }
        else if(PageType.equals("DOWN")){
            tvTittle.setText("New Downtime");
            tvTypeText.setText("Issue:                   ");
            tvPmDuration.setVisibility(View.GONE);
            lblDuration.setVisibility(View.GONE);
        }
        else{
            tvTittle.setText("New Change Over");
            tvTypeText.setText("Change Over               ");
        }
        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        AddPMActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                month = month + 1;
                String date = month + "/" + day+ "/" + year;
                mDisplayDate.setText(date);
            }
        };

        mDisplayTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(AddPMActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);

                        String am_pm;
                        if(calendar.get(Calendar.AM_PM) == Calendar.AM)
                            am_pm = "AM";
                        else
                            am_pm = "PM";

                        String hour = (calendar.get(Calendar.HOUR) == 0) ? "12" : ""+calendar.get(Calendar.HOUR);
                        String minutes = fixZero(calendar.get(Calendar.MINUTE));
                        String time = hour + ":" + minutes + " " + am_pm;

                        mDisplayTime.setText(time);
                    }
                }, hour,minute, false);
                timePickerDialog.show();
            }
        });


        LoadLine();
        btnSavePm.setOnClickListener(this);
        btnBack.setOnClickListener(this);

        //Adding check if line seelected
        spinnerLine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                LoadMachine(spinnerLine.getSelectedItem().toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

    }

    private String fixZero(int num){
        return (num < 10) ? "0" + num : "" + num;
    }

    private  void LoadMachine(String LineName)
    {
        spinnerMachine.setAdapter(null);
        MachineArray.clear();
        RestCrud = APIClient.getClient().create(RestCrud.class);
        LoadActivity.show();
        Call<List<JsonModel3.Machine>> call = RestCrud.GetMachine(LineName);
        myConfig.setTaskListAllMachine(false);
        myConfig.setTaskListPMCO(false);
        call.enqueue(new Callback<List<JsonModel3.Machine>>() {

            @Override
            public void onResponse(Call<List<JsonModel3.Machine>> call, Response<List<JsonModel3.Machine>> response) {
                LoadActivity.dismiss();
                List<JsonModel3.Machine> resource = response.body();

                if(resource!=null){
                    for (int i=0;i<resource.size();i++){
                        MachineArray.add(resource.get(i).StationName);

                    }
                    String[] arraySpinner = new String[MachineArray.size()];
                    for (int i=0;i<MachineArray.size();i++){
                        arraySpinner[i]=MachineArray.get(i);
                    }

                    SetAdap2(arraySpinner);

                }
                myConfig.setTaskListAllMachine(true);
                myConfig.setTaskListPMCO(true);
                Toast.makeText(AddPMActivity.this,"Machine Loaded" , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<JsonModel3.Machine>> call, Throwable t) {
                Toast.makeText(AddPMActivity.this, "ERROR LOADING MACHINE!", Toast.LENGTH_SHORT).show();
                myConfig.setTaskListAllMachine(true);
            }
        });
    }

    private  void LoadLine()
    {
        RestCrud = APIClient.getClient().create(RestCrud.class);

        LoadActivity.show();
        Call<List<JsonModel3.Line>> call = RestCrud.GetLine();


        call.enqueue(new Callback<List<JsonModel3.Line>>() {


            @Override
            public void onResponse(Call<List<JsonModel3.Line>> call, Response<List<JsonModel3.Line>> response) {
                LoadActivity.dismiss();
                List<JsonModel3.Line> resource = response.body();

                if(resource!=null || resource.size() != 0 ){
                    for (int i=0;i<resource.size();i++){
                        String linenamemap = resource.get(i).LineNameMap.replace(".","_");
                        LineArray.add(linenamemap);
                    }
                    String[] arraySpinner = new String[LineArray.size()];
                    for (int i=0;i<LineArray.size();i++){
                        arraySpinner[i]=LineArray.get(i);
                    }

                    SetAdap(arraySpinner);

                }
            }

            @Override
            public void onFailure(Call<List<JsonModel3.Line>> call, Throwable t) {
                Toast.makeText(AddPMActivity.this, "ERROR LOADING LINE!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private  void SetAdap(String[] arraySpinner1){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arraySpinner1);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerLine.setAdapter(adapter);

    }

    private  void SetAdap2(String[] arraySpinner1){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, arraySpinner1);
        adapter.setDropDownViewResource(R.layout.spinner_item);

        spinnerMachine.setAdapter(adapter);

    }


    private void SaveAPM(String StartDate) {

        Log.d("PM ERROR","HERE003");
        String Line = spinnerLine.getSelectedItem().toString();
        String Machine = spinnerMachine.getSelectedItem().toString();
        int Duration = Integer.valueOf(tvPmDuration.getText().toString());
        String PmTYpe = tvPMType.getText().toString();

        PmModel.ModelNewPm modelNewPm = new PmModel().new ModelNewPm();

        modelNewPm.LineName = Line;
        modelNewPm.MachineName = Machine;
        modelNewPm.PmType = PmTYpe;
        modelNewPm.Duration = Duration;
        modelNewPm.StartDate = StartDate;
        modelNewPm.Username = MainActivity.username;

        LoadActivity.show();
        Log.d("PM ERROR","HERE004"+"|"+modelNewPm.LineName+"|"+modelNewPm.MachineName+"|"+modelNewPm.Username+"|"+modelNewPm.StartDate);
        Call<String> call = RestCrud.AddNewPm(modelNewPm);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                LoadActivity.dismiss();
                String dt = response.body().toString();
                Log.d("PM ERROR","HERE005"+dt.toString());
                //Toast.makeText(AddPMActivity.this, dt, Toast.LENGTH_LONG).show();

                Toast.makeText(AddPMActivity.this, "PM ADDED", Toast.LENGTH_LONG).show();
                finish();

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("PM ERROR","HERE007:"+t.getMessage());
                Toast.makeText(AddPMActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                //finish();
            }
        });

    }

    private void SaveAPM2(String StartDate) {

        String Line = spinnerLine.getSelectedItem().toString();
        String Machine = spinnerMachine.getSelectedItem().toString();

        String PmTYpe = tvPMType.getText().toString();

        CoModel.ModelNewCo modelNewCo = new CoModel().new ModelNewCo();


        if(Line.isEmpty() ||tvPmDuration.getText().toString().isEmpty() || Machine.isEmpty() || StartDate.isEmpty() || PmTYpe.isEmpty()){
            Toast.makeText(AddPMActivity.this,"PLease Enter All details",Toast.LENGTH_SHORT).show();
            return;
        }

        int Duration = Integer.valueOf(tvPmDuration.getText().toString());

        modelNewCo.LineName = Line;
        modelNewCo.MachineName = Machine;
        modelNewCo.CoType = PmTYpe;
        modelNewCo.Duration = Duration;
        modelNewCo.StartDate = StartDate;
        modelNewCo.Username = MainActivity.username;



        Call<String> call = RestCrud.AddNewCo(modelNewCo);
        LoadActivity.show();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                LoadActivity.dismiss();
                String dt = response.body().toString();

                if(dt.equals("Success"))
                {
                    Toast.makeText(AddPMActivity.this, "CO ADDED", Toast.LENGTH_LONG).show();
                    myConfig.setCoCurrentPage("MainPage");
                    finish();
                }
                else{
                    Toast.makeText(AddPMActivity.this, "ERROR Adding Co here"+dt, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("PM ERROR",t.getMessage().toString());
                Toast.makeText(AddPMActivity.this, "ERROR CREATING NEWa CO"+t.getMessage(), Toast.LENGTH_LONG).show();
                finish();
            }
        });

    }

    private void SaveDownTime(String StartDate) {

        String Line = spinnerLine.getSelectedItem().toString();
        String Machine = spinnerMachine.getSelectedItem().toString();
        String PmTYpe = tvPMType.getText().toString();

        DownTimeModel.ModelAddCloseDowntime modelNewDown = new DownTimeModel().new ModelAddCloseDowntime();


        if(Line.isEmpty() || Machine.isEmpty() || StartDate.isEmpty() || PmTYpe.isEmpty()){
            Toast.makeText(AddPMActivity.this,"PLease Enter All Details",Toast.LENGTH_SHORT).show();
            return;
        }

        modelNewDown.LineNameMap = Line;
        modelNewDown.StationName = Machine;
        modelNewDown.CoType = "DOWNTIME";
        modelNewDown.StartDate = StartDate;
        modelNewDown.Others = "Issue:"+PmTYpe;
        modelNewDown.PIC = MainActivity.username;



        Call<String> call = RestCrud.AddNewDownTime(modelNewDown);
        LoadActivity.show();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                LoadActivity.dismiss();
                String dt = response.body().toString();

                if(dt.contains("debug"))
                {
                    Toast.makeText(AddPMActivity.this, "Debug:"+dt, Toast.LENGTH_LONG).show();
                    myConfig.setCoCurrentPage("MainPage");
                    finish();
                }

                if(dt.contains("true"))
                {
                    Toast.makeText(AddPMActivity.this, "DOWNTIME ADDED", Toast.LENGTH_LONG).show();
                    myConfig.setCoCurrentPage("MainPage");
                    finish();
                }
                else{
                    Toast.makeText(AddPMActivity.this, "ERROR Adding Downtime "+dt, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                LoadActivity.dismiss();
                Toast.makeText(AddPMActivity.this, "ERROR CREATING NEW Downtime"+t.getMessage(), Toast.LENGTH_LONG).show();
                finish();
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnBack:
                finish();
                break;

            case R.id.btnSavePm:
                String time = (String) mDisplayTime.getText();
                String date = (String) mDisplayDate.getText();
                String _date = date.replace("/", "-");
                String time24="";

                if(time.isEmpty() || date.isEmpty()){
                    Toast.makeText(AddPMActivity.this,"Please Add Date and Time",Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
                    SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
                    time24 = outFormat.format(inFormat.parse(time));
                    time24 = time24.replace(":", "-");
                }
                catch (ParseException e)
                {
                    Log.d("PM ERROR",e.getMessage());
                }
                String StartDate=_date +" "+time24;

                Log.d("PM ERROR","HERE001");
                if(PageType.equals("PM")){
                    Log.d("PM ERROR","HERE002");
                    SaveAPM(StartDate);
                }
                else if(PageType.equals("DOWN")){
                    SaveDownTime(StartDate);
                }
                else {
                    SaveAPM2(StartDate);
                }


                break;

        }

    }
}
