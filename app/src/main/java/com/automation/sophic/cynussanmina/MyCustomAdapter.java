package com.automation.sophic.cynussanmina;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Vibrator;
import android.text.Html;
import android.text.Layout;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.automation.sophic.cynussanmina.Model.JsonModel3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by X13 on 2/4/2017.
 */

public class MyCustomAdapter extends BaseAdapter implements ListAdapter {
    private ArrayList<String> list = new ArrayList<String>();
    private Context context;
    TableDisplay_Popup add;
    DisplayDetails_Popup Info;
    private Vibrator vibrator;
    private String getnewalert;
    RestCrud RestCrud;
    RelativeLayout layout;
    Button btn_Accept;
    Button btn_Info;
    TextView listItemText,listItemText2;

    public MyCustomAdapter(ArrayList<String> list,String getnewalert, Context context) {
        this.list = list;
        this.getnewalert = getnewalert;
        this.context = context;
        vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
        //just return 0 if your list items do not have an Id variable.
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_layout2, parent,false);
        }
        final String[] rowData = list.get(position).split(Pattern.quote("|"));
        final String[] AlertMessage = rowData[0].split(Pattern.quote("~"));
        layout = (RelativeLayout)view.findViewById(R.id.RelLayout1);

        //Handle TextView and display string from your list
        listItemText = (TextView)view.findViewById(R.id.itemlist);
        listItemText2 = (TextView)view.findViewById(R.id.itemlist2);
        TextView tvMachine = (TextView)view.findViewById(R.id.tvMachine);
        TextView tvLine = (TextView)view.findViewById(R.id.tvLine);
        TextView tvModule = (TextView)view.findViewById(R.id.tvModule);
        TextView tvWaitingTime = (TextView)view.findViewById(R.id.tvWaitingTime);
        btn_Accept = (Button)view.findViewById(R.id.book);
        btn_Info   = (Button)view.findViewById(R.id.btnInfo);

        //for cynus
        btn_Accept.setHeight(110);
        btn_Info.setHeight(110);

        String text = "";

        listItemText.setSingleLine(false);
        Log.d("here", String.valueOf(AlertMessage.length));
        tvMachine.setText(AlertMessage[1]);
        tvLine.setText(AlertMessage[2]);
        tvModule.setText(AlertMessage[3]);

        //get wainting time

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateInString = "";

        if(AlertMessage.length>10){
            if(AlertMessage[11].equals("SYS")){
                dateInString =  AlertMessage[12];
            }
            else {
                dateInString =  AlertMessage[13];
            }

            if(getnewalert.equals("true"))
            {
                dateInString =  AlertMessage[12];
            }
            else{
                dateInString =  AlertMessage[13];
            }

        }
        else {

            if(AlertMessage[7].equals("SYS")){
                dateInString =  AlertMessage[8];
            }
            else {
                dateInString =  AlertMessage[8];
            }

            if(getnewalert.equals("true"))
            {
                dateInString =  AlertMessage[8];
            }
            else{
                dateInString =  AlertMessage[9];
            }

        }






        dateInString=dateInString.replace("T"," ");
        try {
            dateInString = outFormat.format(formatter.parse(dateInString));
        } catch (Exception e) {
            e.printStackTrace();
        }



        Date currentDate = new Date();
        try {
            Date StartDate = formatter.parse(dateInString);
            long diff = currentDate.getTime() - StartDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            tvWaitingTime.setText(String.valueOf(minutes)+"(min)");


        } catch (ParseException e) {
            e.printStackTrace();
        }




        listItemText2.setHeight(80);

        if(AlertMessage.length>10){

             text =
                     "<font color='black'>[CPK6]</font>     "+
                             "<font color='black'>[CPK4]</font>     "+
                             "<font color='black'>[CPK2]</font>     "+
                             "<font color='black'>[CPKC]</font>     "+
                             "<font color='black'>[TRN]</font><br>"+

                     "<font color='black'>["+AlertMessage[4]+"]</font>     "+
                    "<font color='black'>["+AlertMessage[5]+"]</font>     "+
                    "<font color='black'>["+AlertMessage[6]+"]</font>     "+
                     "<font color='black'>["+AlertMessage[7]+"]</font>     ";

            if(AlertMessage[8].equals(">"))
                text += "[<font color='green'>&uarr;</font>]";
            else if (AlertMessage[8].equals("<"))
                text += "[<font color='red'>&darr;</font>]";
            else if (AlertMessage[8].equals("="))
                text += "[<font color='yellow'>"+AlertMessage[8]+"</font>]";
            else
                text += "[<font color='black'>"+AlertMessage[8]+"</font>]";


            //RelativeLayout.LayoutParams Params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            listItemText2.setBackgroundColor(Color.parseColor("#ae3fc1"));
            //Params1.width =410;
            //Params1.height =100;
           // Params1.addRule(RelativeLayout.BELOW, R.id.tvMachine);
            //Params1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            //listItemText.setLayoutParams(Params1);
            listItemText2.setTextSize(TypedValue.COMPLEX_UNIT_SP,19);


            AlertMessage[5]=  AlertMessage[9];
            AlertMessage[6]=  AlertMessage[10];
            AlertMessage[7]=  AlertMessage[11];
        }
        else {
            RelativeLayout.LayoutParams Params1 = new RelativeLayout.LayoutParams(420,70);
            Params1.addRule(RelativeLayout.BELOW, R.id.tvMachine);
            listItemText2.setBackgroundColor(Color.parseColor("#dc01a5"));
            listItemText2.setTextSize(TypedValue.COMPLEX_UNIT_SP,22);
            //listItemText2.setLayoutParams(Params1);
            text = "<font color='black'>Code: " + AlertMessage[4] + "</font>" + "\n";
        }

        listItemText2.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
        //listItemText.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
      // listItemText.setText(text);


        //Handle buttons and add onClickListeners
        final Button btn_Accept = (Button)view.findViewById(R.id.book);
        btn_Accept.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                add=new TableDisplay_Popup((table_display) context,(table_display) context,rowData,btn_Accept,btn_Accept.getText().toString(),AlertMessage);
                //add = new NewAlert_Display_Popup((table_display) context,(table_display) context,rowData,btn_Accept);
                add.show();


                add.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(final DialogInterface arg0) {
                        int stat = add.isOkpressed();

                        if(stat == 1){
                            Toast.makeText(context, "Updating Data Please Wait !" , Toast.LENGTH_SHORT).show();
                            JsonModel3.AlertDone alertDone = new JsonModel3.AlertDone();
                            Call<String> call;
                            RestCrud = APIClient.getClient().create(RestCrud.class);
                            String body = MainActivity.username;
                            Call<String> call2 = null;

                            if(btn_Accept.getText().equals("Done")){
                                String Message = add.getMessage();


                                alertDone.AlertId=AlertMessage[0];
                                alertDone.PICName= MainActivity.username;
                                alertDone.ActionTaken=Message;

                                call = RestCrud.setDone(alertDone);
                                call.enqueue(new Callback<String>() {
                                    @Override
                                    public void onResponse(Call<String> call, Response<String> response) {
                                        Toast.makeText(context, "Data Updated!" , Toast.LENGTH_SHORT).show();

                                    }

                                    @Override
                                    public void onFailure(Call<String> call, Throwable t) {
                                        Toast.makeText(context, "Error Updating Data" , Toast.LENGTH_SHORT).show();

                                    }
                                });
                                //Toast.makeText(context, AlertMessage[0]+"|"+body+"|"+Message+"DONE !", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                int CurrentCount = table_display.Total();
                                CurrentCount--;
                                table_display.UpdateTotal(CurrentCount);
                                call2 = RestCrud.setAck(AlertMessage[0]+"|"+body);

                                call2.enqueue(new Callback<String>() {
                                    @Override
                                    public void onResponse(Call<String> call, Response<String> response) {

                                        Log.d("err","sucess");
                                        list.remove(position); //or some other task
                                        notifyDataSetChanged();
                                        vibrator.vibrate(100);
                                    }

                                    @Override
                                    public void onFailure(Call<String> call, Throwable t) {
                                        Log.d("err",t.toString());
                                        call.cancel();
                                    }
                                });
                            }



                        }
                        else if (stat == -1){
                            Toast.makeText(context, "Please Select Message!", Toast.LENGTH_SHORT).show();
                        }
                        else if (stat == -2){
                            Toast.makeText(context, "Please Input Other Message!", Toast.LENGTH_SHORT).show();
                        }
                        else{

                        }
                    }
                });
                //do something


            }
        });


        //Handle buttons and add onClickListeners
        final Button btn_Info = (Button)view.findViewById(R.id.btnInfo);
        btn_Info.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Info=new DisplayDetails_Popup((table_display) context,(table_display) context,AlertMessage,btn_Accept);

                Info.show();


                Info.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(final DialogInterface arg0) {
                        boolean stat = Info.isOkpressed();

                        if(stat){


                        }
                        else{

                        }
                    }
                });
                //do something


            }
        });


        if(getnewalert.equals("false")){
            //btn_Accept.setVisibility(View.GONE);
            btn_Accept.setText("Done");
           /* RelativeLayout.LayoutParams params =
                    new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.MATCH_PARENT);


            listItemText.setLayoutParams(params);*/
        }
        else{
            btn_Accept.setVisibility(View.VISIBLE);
        }

        return view;
    }
}
