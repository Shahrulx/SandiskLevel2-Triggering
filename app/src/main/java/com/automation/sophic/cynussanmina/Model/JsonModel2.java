package com.automation.sophic.cynussanmina.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by X-PC on 29/1/2018.
 */

public class JsonModel2 {

    @SerializedName("Id")
    public String Id;
    @SerializedName("DateTime")
    public String DateTime;
    @SerializedName("LineId")
    public String LineId;
    @SerializedName("MachineId")
    public String MachineId;
    @SerializedName("PmType")
    public String PmType;
    @SerializedName("Duration")
    public String Duration;
    @SerializedName("EndTime")
    public String EndTime;
    @SerializedName("PIC")
    public String PIC;
}
