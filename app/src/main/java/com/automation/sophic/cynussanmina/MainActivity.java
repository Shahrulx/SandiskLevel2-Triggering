package com.automation.sophic.cynussanmina;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Vibrator;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.automation.sophic.cynussanmina.Helper.ConfirmDialog;
import com.automation.sophic.cynussanmina.Model.JsonModel3;
import com.automation.sophic.cynussanmina.Model.MyConfig;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    //p
    private Intent intent;
    public static boolean isSanminaWeb= false;
    public static String ZoneSelected= "";
    private NetworkCallback netCallback;
    private NetworkTask netTask;
    public static String username;
    String password="";
    EditText txtusername;
    EditText txtpassword;

    private Vibrator vibrator;
    String Page = "Main";
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String URL = "URL";
    SharedPreferences sharedpreferences;
    MyConfig myConfig = MyConfig.getInstance();
    public static RestCrud RestCrud;
    boolean CheckDone=false;

    @Override
    protected void onResume(){
        super.onResume();
        CheckUpdate();
        txtusername.setText("");
        txtpassword.setText("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.activity_main);
        vibrator = (Vibrator)this.getSystemService(Context.VIBRATOR_SERVICE);
        MyConfig mc = MyConfig.getInstance();
        txtusername = (EditText) findViewById(R.id.text_username);
        txtpassword = (EditText) findViewById(R.id.txt_Password);
        TextView tvVersionNumber = (TextView) findViewById(R.id.tvVersionNumber);
        tvVersionNumber.setText("Ver:"+BuildConfig.VERSION_NAME);



        netCallback = new NetworkCallback() {
            @Override
            public void requestDone(String result) {
                Log.d("login", "requestDone: "+result);

                if(result.toString().contains("True,") ){
                    String currentVersion = BuildConfig.VERSION_NAME;
                    result = result.replace(" ", "");
                    result = result.substring(0, result.length() - 1);
                    result = result.substring(1);

                    String[] UpdateVer = result.split(",");
                    if(!currentVersion.contains(UpdateVer[1])){
                        Toast.makeText(MainActivity.this, "Found Update Please Restart App!", Toast.LENGTH_LONG).show();
                        Close();
                    }
                    else {

                    }

                }
                else  if(result.toString().contains("False,") ){

                }
                else {


                    if(result.contains("0")){
                        Toast.makeText(MainActivity.this, "Incorrect username !", Toast.LENGTH_SHORT).show();
                    }
                    else if(result.toString().equals("-1")){
                        Toast.makeText(MainActivity.this, "Network Error!", Toast.LENGTH_SHORT).show();
                        Toast.makeText(MainActivity.this,  APIClient.URL, Toast.LENGTH_SHORT).show();

                    }
                    else if(result.contains("1")){

                        if(Page.equals("Main")){
                            //intent = new Intent(MainActivity.this, table_display.class);
                            intent = new Intent(MainActivity.this, ListAllMachine.class);
                            intent.putExtra("Type","LinePage");
                        }

                        else
                        {

                           // intent = new Intent(MainActivity.this, ListAllMachine.class);
                            //intent.putExtra("MachineName", "M1");
                        }

                        RestCrud = APIClient.getClient().create(RestCrud.class);
                        startActivity(intent);
                    }
                }

                CheckDone=true;
            }


        };

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        try{
            APIClient.URL = "http://"+sharedpreferences.getString(URL, null).toString()+"/";
        }
        catch (Exception ex){
            SharedPreferences.Editor editor = sharedpreferences.edit();
            String restoredText = sharedpreferences.getString(URL, null);

            editor.putString(URL, "URL");
            editor.commit();
            APIClient.URL = "http://"+sharedpreferences.getString(URL, null).toString()+"/";
        }

       // Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Welcome To Main Activity", Snackbar.LENGTH_LONG);
        //snackbar.show();
        Initialization();


    }

    public void Close(){
        this.finishAffinity();
    }

    //We will use this method to build our URL string and then pass it to the network task object
    //to execute it in the background.
    //When we receive a reply then
    private void authenticate(String userName,String Password){
        String URL = "";


        isSanminaWeb = true;
        URL = APIClient.URL+"RestApi/GetUser/"+userName+","+Password;
       // Toast.makeText(MainActivity.this,URL, Toast.LENGTH_LONG).show();
        netTask = new NetworkTask();
        netTask.setCallback(netCallback);
        netTask.execute(URL);
    }

    private void CheckUpdate(){
        String URL2 = "";
        Log.d("login","HERE");
        if(!APIClient.URL.contains("http://")){
            URL2 = "http://";
        }
        URL2 =URL2+ APIClient.URL+"/RestApi/CheckUpdate";
        Log.d("login","HERE"+URL2);
        Toast.makeText(MainActivity.this,"AA"+URL2,Toast.LENGTH_SHORT);
        netTask = new NetworkTask();
        netTask.setCallback(netCallback);
        netTask.execute(URL2);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("DEBUG HERE","Destroy called");

    }

    @Override
    public void onBackPressed() {
        final ConfirmDialog cdd=new ConfirmDialog(MainActivity.this,"Confirm Exit?");

        cdd.show();

        cdd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(final DialogInterface arg0) {
                int stat = cdd.isOkpressed();

                if(stat==1){
                    finish();
                }
                else{

                }
            }
        });
    }

    private void callLoginDialog()
    {
        Dialog myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.login_popup);
        myDialog.setCancelable(false);
        Button login = (Button) myDialog.findViewById(R.id.btn_PassLogin);


        myDialog.show();

        login.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                //your login calculation goes here
            }
        });

    }



    private  void Initialization(){
        final EditText text_username = (EditText) findViewById(R.id.text_username);
        final EditText txt_Password =  (EditText) findViewById(R.id.txt_Password);
        final Button btn_setting = (Button) findViewById(R.id.btnSetting);

        final Button btn_PassPMLogin = (Button) findViewById(R.id.btn_PassPMLogin);
        btn_PassPMLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                username = text_username.getText().toString();
                password = txt_Password.getText().toString();
                if(username.equals("1"))
                    isSanminaWeb=false;
                else
                    isSanminaWeb = false;


                if(username.equals("") || password.equals("")){
                    Toast.makeText(MainActivity.this, "Please Enter UserName Password....", Toast.LENGTH_LONG).show();
                    return;
                }
                Page="Main";



                CheckDone=false;

                authenticate(username,password);


            }
        });




        btn_setting.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Please wait loading data....", Toast.LENGTH_LONG).show();
                intent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(intent);

            }
        });


        // final Spinner spinner = (Spinner) findViewById(R.id.spinner_zone);

        List<String> list = new ArrayList<String>();
        list.add("A");
        list.add("Z1");
        list.add("Z2");
        list.add("Z3");
        list.add("Z4");


        list.add("[Select Zone]");
        final int listsize = list.size() - 1;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, list) {
            @Override
            public int getCount() {
                return(listsize); // Truncate the list
            }
        };
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

    }



}
