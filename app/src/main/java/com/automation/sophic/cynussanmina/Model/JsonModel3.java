package com.automation.sophic.cynussanmina.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by X-PC on 30/1/2018.
 */

public class JsonModel3 {

    public class Line
    {
        @SerializedName("LineNameMap")
        public String LineNameMap;
    }

    public class LineByMachine
    {
        public int ErrorCount;
        public String LineName;
        public String OtherParam;
        public  String LineId;
    }

    public class Machine
    {
        @SerializedName("StationName")
        public String StationName;
    }

    public static class MachineList
    {
        @SerializedName("MachineName")
        public String MachineName;
    }

    public static class AlertDone
    {
        public String AlertId;
        public String PICName;
        public String ActionTaken;

    }

    public class AllMachineList
    {
        public String MachineType;
    }



}
