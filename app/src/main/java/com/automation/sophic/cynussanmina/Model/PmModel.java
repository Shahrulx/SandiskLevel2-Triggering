package com.automation.sophic.cynussanmina.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by X-PC on 20/2/2018.
 */

public class PmModel {

    public class GetPmLine
    {
        public int c;
        public String LineNameMap;
        public int Id;
    }

    public class GetPmMachine
    {
        public int Id ;
        public int PIC ;
        public int MachineId;
        public int Duration ;
        public String PmType;
        public String StationName ;
        public String LineNameMap ;
        public String DateTime;
        public String EndTime ;
    }

    public class GetPmMachineDetail
    {
        public int Id  ;
        public String StartDate;
        public int PIC;
        public int MachineId;
        public int Duration;
        public String PmType;
        public String StationNameMap;
        public String LineNameMap;
        public String DateTime;
    }

    public class ModelNewPm
    {
        public String LineName ;
        public String MachineName;
        public String PmType;
        public int Duration;
        public String StartDate;
        public String Username;
    }

    public class PmCheckList
    {
        public int Id;
        public String DateCreated ;
        public int PmId;
        public int MachineId ;
        public String ChecklistId ;
        public String Others ;
        public String EndTime;
        public String PIC ;
    }
}
