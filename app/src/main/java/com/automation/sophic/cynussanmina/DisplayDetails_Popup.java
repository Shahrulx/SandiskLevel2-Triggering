package com.automation.sophic.cynussanmina;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class DisplayDetails_Popup extends Dialog implements
        View.OnClickListener  {


    private Button btn_save,btn_select_date;
    TextView txtMachine,txtSlot,txtUserId,txtPartNumber,txtPer,txtLength,txtTime,txtTrolley;
    TextView tvRemedy,tvCause,tvInfo;
    //Button btn_cancel,btn_pick_book;
    //Button btn_book_temp;
    private table_display tableDisplay;
    String[] Cells;
    String buttonContent;
    private NetworkTask netTask;
    private NetworkCallback netCallbackBook;
    private NetworkCallback netCallbackPick;

    public boolean isOkpressed() {
        return Okpressed;
    }

    public void setOkpressed(boolean okpressed) {
        Okpressed = okpressed;
    }

    public boolean Okpressed;

    public Activity activity;

    //public TableDisplay_Popup(table_display tableDisplay,Activity a,String[] cells,Button btn) {
    public DisplayDetails_Popup(table_display tableDisplay, Activity a, String[] cells, Button btn) {
        super(a);
        // TODO Auto-generated constructor stub
        this.activity = a;
        this.Cells = cells;
        //this.btn_book_temp = btn;
        this.buttonContent = btn.getText().toString();
        this.tableDisplay = tableDisplay;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       requestWindowFeature(Window.FEATURE_NO_TITLE);
       setContentView(R.layout.layout_detailview);

        //btn_pick_book=(Button) findViewById(R.id.btn_pick_book);
        //btn_pick_book.setText(buttonContent);
        tvInfo=(TextView) findViewById(R.id.tvInfo);
        tvRemedy=(TextView) findViewById(R.id.tvRemedy);
        tvCause=(TextView) findViewById(R.id.tvCause);
        //btn_cancel=(Button) findViewById(R.id.btn_cancel);
        tvRemedy.setText(Cells[6]);
        tvCause.setText(Cells[5]);
        String Username = "";



        if(Username.equals("null")){
            Username = "";
        }
        else{
            if(Cells.length == 8){
                Username = Cells[7];
            }

        }


        tvInfo.setText("Assigned To:"+Username);
        //btn_cancel.setOnClickListener(this);
        //btn_pick_book.setOnClickListener(this);
    }

    public void Initialize(){

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel:
                setOkpressed(false);
                dismiss();
                break;

            case R.id.btn_pick_book:
                setOkpressed(true);
                dismiss();
                break;
        }
    }

}
