package com.automation.sophic.cynussanmina;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by andrei on 2/27/17.
 */

public class DataAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private  ArrayList<String> mValues;
    private Vibrator vibrator;
    View rowView;
    TableDisplay_Popup add;

    public DataAdapter(Context context, ArrayList<String> objects) {
        super(context, -1, objects);
        mContext = context;
        mValues = objects;
        vibrator = (Vibrator)mContext.getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rowView = inflater.inflate(R.layout.row_layout2, parent, false);
        TextView no = (TextView) rowView.findViewById(R.id.itemlist);
        final Button book = (Button) rowView.findViewById(R.id.book);
        final String[] rowData = mValues.get(position).split(Pattern.quote("|"));
        final String test[] = {};
        no.setText(rowData[0]);
        no.setHeight(100);

       no.setTextSize(16);
//        per.setTextSize(16);
//        length.setTextSize(16);
//        time.setTextSize(16);

        book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add=new TableDisplay_Popup((table_display) mContext,(table_display) mContext,rowData,book,"",test);

                add.show();


                add.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(final DialogInterface arg0) {
                        int stat = add.isOkpressed();

                        if(stat==1){
                            //vibrator.vibrate(100);
                        }
                        else{
                            //vibrator.vibrate(500);
                        }
                    }
                });



            }
        });

        return  rowView;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }
}
