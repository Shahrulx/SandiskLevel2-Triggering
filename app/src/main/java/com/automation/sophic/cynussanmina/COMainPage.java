package com.automation.sophic.cynussanmina;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.automation.sophic.cynussanmina.Helper.LoadingClass;
import com.automation.sophic.cynussanmina.Model.CoModel;
import com.automation.sophic.cynussanmina.Model.MyConfig;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class COMainPage extends AppCompatActivity implements
        android.view.View.OnClickListener {

    TableLayout layoutMachineList;
    RestCrud RestCrud;
    MyConfig myConfig = MyConfig.getInstance();
    int CureentData=0;
    int PrevData=0;
    String PageType="";
    String LineId="";
    Button btnBack,btnNewPm;
    TextView tvTittle;
    boolean isCreated = true;
    LoadingClass LoadActivity;

    @Override
    public void onDestroy() {
        Log.d("TimerLog","PmListTimer Stop");
        //myConfig.CoListTimer.cancel();
        super.onDestroy();
    }

    @Override
    public void onResume(){
        super.onResume();
        //myConfig.PmListTimer.cancel();
        //CreateTimer();
        Log.d("PMSTAT","Resume:  "+PageType);

        if(isCreated==false){

        }
        else {

            if(PageType.equals("LinePage"))
            {
                AddData(true);
            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_pmline);
        layoutMachineList = (TableLayout) findViewById(R.id.layoutMachineList);
        btnBack = (Button) findViewById(R.id.btnBack);
        btnNewPm = (Button) findViewById(R.id.btnNewPm);
        tvTittle = (TextView) findViewById(R.id.tvStartDate);
        tvTittle.setText("Change Over");

        btnBack.setOnClickListener(this);
        //btnNewPm.setVisibility(View.INVISIBLE);
        btnNewPm.setText("NEW CO");
        tvTittle.setBackgroundResource(R.drawable.borderlinename);
        tvTittle.setTextColor(getResources().getColor(R.color.white));
        tvTittle.setTypeface(null, Typeface.BOLD);
        tvTittle.setPadding(10,8,10,8);
        btnNewPm.setOnClickListener(this);

        PageType=getIntent().getStringExtra("Type");
        RestCrud = APIClient.getClient().create(RestCrud.class);
        LoadActivity = new LoadingClass(this,1);
        ClearTable();

        if(PageType.equals("MachinePage")){
            LineId=getIntent().getStringExtra("LineId");
            String Content=getIntent().getStringExtra("LineName");

            Log.d("IMHERE",Content);
            tvTittle.setText(Content);
            GetPmMachine(LineId,false);
        }
        else{
            AddData(true);
        }

        isCreated = true;
    }


    public void GetPmMachine(String LineId,final boolean Init) {

        Call<List<CoModel.GetCoMachine>> call = RestCrud.GetCoMachine(LineId);
        LoadActivity.show();
        call.enqueue(new Callback<List<CoModel.GetCoMachine>>() {

            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<List<CoModel.GetCoMachine>> call, Response<List<CoModel.GetCoMachine>> response) {

                LoadActivity.dismiss();
                List<CoModel.GetCoMachine> body = response.body();
                if (body == null) {
                    return;
                }

                Log.d("Res",response.body().toString());

                List<CoModel.GetCoMachine> resource = response.body();
                if(resource == null)
                    return;

                int numofdata = resource.size();
                CureentData = numofdata;

                if(Init)
                {
                    PrevData=CureentData;
                }
                else if(PrevData==CureentData)
                {
                    return;
                }

                else
                {
                    PrevData=CureentData;
                }

                int i = 0;

                ClearTable();

                if (numofdata > 0) {

                    int remainder=0;
                    int rownum=0;
                    int index = numofdata%4;
                    int toloop=0;

                    if(numofdata<=4){
                        toloop=numofdata;
                        rownum=1;
                    }

                    else if(index==0){
                        toloop=4;
                        rownum=numofdata/4;
                    }

                    else{
                        toloop=4;
                        rownum =numofdata/4;
                        remainder = numofdata-(4*rownum);
                    }
                    int pos=0;
                    for (int j=0;j<rownum;j++)
                    {
                        TableRow row = new TableRow(COMainPage.this);
                        for (i=0; i < toloop; i++) {
                            String LineNameMap= resource.get(pos).StationName;
                            int c= resource.get(pos).Id;
                            int Id= resource.get(pos).Id;

                            Button btn = new Button(COMainPage.this);

                            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 100, getResources().getDisplayMetrics());
                            TableRow.LayoutParams lp = new TableRow.LayoutParams(px,px);
                            lp.setMargins(0,15,15,0);
                            //btn.setBackgroundColor(Color.BLUE); // From android.graphics.Color

                            ObjectAnimator anim = ObjectAnimator.ofInt(btn,"backgroundColor",Color.parseColor("#2433ff"),Color.parseColor("#000ba4"),Color.parseColor("#2433ff"));
                            anim.setDuration(1000);
                            anim.setEvaluator(new ArgbEvaluator());
                            anim.setRepeatMode(Animation.REVERSE);
                            anim.setRepeatCount(Animation.INFINITE);
                            anim.start();

                            btn.setTextColor(Color.WHITE); //SET CUSTOM COLOR
                            btn.setLayoutParams(lp);
                            btn.setText(LineNameMap+"\n"+c);
                            btn.setTag(Id);
                            btn.setOnClickListener(COMainPage.this);

                            row.addView(btn);
                            pos++;
                        }
                        layoutMachineList.addView(row);
                    }

                    if(remainder!=0){

                        TableRow row = new TableRow(COMainPage.this);
                        for (i=0; i < remainder; i++) {
                            String LineNameMap= resource.get(i).StationName;
                            int c= resource.get(pos).Id;
                            int Id= resource.get(pos).Id;

                            Button btn = new Button(COMainPage.this);

                            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 100, getResources().getDisplayMetrics());
                            TableRow.LayoutParams lp = new TableRow.LayoutParams(px,px);
                            lp.setMargins(0,15,15,0);
                            //btn.setBackgroundColor(Color.BLUE); // From android.graphics.Color
                            ObjectAnimator anim = ObjectAnimator.ofInt(btn,"backgroundColor",Color.parseColor("#2433ff"),Color.parseColor("#000ba4"),Color.parseColor("#2433ff"));
                            anim.setDuration(1000);
                            anim.setEvaluator(new ArgbEvaluator());
                            anim.setRepeatMode(Animation.REVERSE);
                            anim.setRepeatCount(Animation.INFINITE);
                            anim.start();
                            btn.setTextColor(Color.WHITE); //SET CUSTOM COLOR
                            btn.setLayoutParams(lp);
                            btn.setText(LineNameMap+"\n"+c);
                            btn.setTag(Id);
                            btn.setOnClickListener(COMainPage.this);

                            row.addView(btn);
                            pos++;
                        }
                        layoutMachineList.addView(row);
                    }

                }
            }

            @Override
            public void onFailure(Call<List<CoModel.GetCoMachine>> call, Throwable t) {
                LoadActivity.dismiss();
                Log.d("err","error get");
            }
        });

    }

    public void AddData(final boolean Init) {

        Call<List<CoModel.GetCoLine>> call = RestCrud.GetCoLine();
        LoadActivity.show();
        call.enqueue(new Callback<List<CoModel.GetCoLine>>() {


            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<List<CoModel.GetCoLine>> call, Response<List<CoModel.GetCoLine>> response) {

                LoadActivity.dismiss();
                List<CoModel.GetCoLine> body = response.body();
                if (body == null) {
                    return;
                }

                Log.d("Res",response.body().toString());

                List<CoModel.GetCoLine> resource = response.body();


                if(resource == null)
                    return;

                int numofdata = resource.size();
                CureentData = numofdata;

                if(Init)
                {
                    PrevData=CureentData;
                }

                else if(PrevData==CureentData){
                    return;
                }

                else
                {
                    PrevData=CureentData;
                }


                ClearTable();


                int i = 0;

                if (numofdata > 0) {


                    int remainder=0;
                    int rownum=0;
                    int index = numofdata%4;
                    int toloop=0;

                    if(numofdata<=4){
                        toloop=numofdata;
                        rownum=1;
                    }
                    else if(index==0){
                        toloop=4;
                        rownum=numofdata/4;
                    }
                    else{
                        toloop=4;
                        rownum =numofdata/4;
                        remainder = numofdata-(4*rownum);
                    }

                    TableRow row;

                    for (int j=0;j<rownum;j++){

                        row = new TableRow(COMainPage.this);

                        for (i=0; i < toloop; i++) {
                            String LineNameMap= resource.get(i).LineNameMap;
                            int c= resource.get(i).c;
                            int Id= resource.get(i).Id;

                            Button btn = new Button(COMainPage.this);

                            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 100, getResources().getDisplayMetrics());
                            TableRow.LayoutParams lp = new TableRow.LayoutParams(px,px);
                            lp.setMargins(30,15,25,0);
                            //btn.setBackgroundColor(Color.BLUE); // From android.graphics.Color

                            ObjectAnimator anim = ObjectAnimator.ofInt(btn,"backgroundColor",Color.parseColor("#2433ff"),Color.parseColor("#000ba4"),Color.parseColor("#2433ff"));
                            anim.setDuration(1000);
                            anim.setEvaluator(new ArgbEvaluator());
                            anim.setRepeatMode(Animation.REVERSE);
                            anim.setRepeatCount(Animation.INFINITE);
                            anim.start();

                            btn.setTextColor(Color.WHITE); //SET CUSTOM COLOR
                            btn.setLayoutParams(lp);
                            btn.setText(LineNameMap+"\n"+c);
                            btn.setTag(Id);
                            btn.setOnClickListener(COMainPage.this);

                            row.addView(btn);
                        }
                        layoutMachineList.addView(row);
                    }

                    if(remainder!=0){
                        int _remainder = numofdata-remainder;
                        row = new TableRow(COMainPage.this);

                        for(int i2=_remainder;i2<numofdata;i2++){
                            String LineNameMap= resource.get(i2).LineNameMap;
                            int c= resource.get(i2).c;
                            int Id= resource.get(i2).Id;

                            Button btn = new Button(COMainPage.this);

                            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 100, getResources().getDisplayMetrics());
                            TableRow.LayoutParams lp = new TableRow.LayoutParams(px,px);
                            lp.setMargins(30,15,25,0);
                            //btn.setBackgroundColor(Color.BLUE); // From android.graphics.Color

                            ObjectAnimator anim = ObjectAnimator.ofInt(btn,"backgroundColor",Color.parseColor("#2433ff"),Color.parseColor("#000ba4"),Color.parseColor("#2433ff"));
                            anim.setDuration(1000);
                            anim.setEvaluator(new ArgbEvaluator());
                            anim.setRepeatMode(Animation.REVERSE);
                            anim.setRepeatCount(Animation.INFINITE);
                            anim.start();

                            btn.setTextColor(Color.WHITE); //SET CUSTOM COLOR
                            btn.setLayoutParams(lp);
                            btn.setText(LineNameMap+"\n"+c);
                            btn.setTag(Id);
                            btn.setOnClickListener(COMainPage.this);

                            row.addView(btn);
                        }
                        layoutMachineList.addView(row);
                    }




                }
            }

            @Override
            public void onFailure(Call<List<CoModel.GetCoLine>> call, Throwable t) {
                Log.d("err","error get");
            }
        });
    }

    private void ClearTable()
    {
        layoutMachineList.removeAllViews();
    }

    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnBack:
                if(myConfig.getCoCurrentPage() == "CoMachine"){
                    myConfig.setCoCurrentPage("CoLine");
                }
                else if(myConfig.getCoCurrentPage() == "CoLine"){
                    myConfig.setCoCurrentPage("CoMachine");
                }
                else{
                    myConfig.setCoCurrentPage("MainPage");
                }

                finish();
                break;

            case R.id.btnNewPm:
                Intent AddPMActivity = new Intent(COMainPage.this,AddPMActivity.class);
                AddPMActivity.putExtra("Type","CO");
                startActivity(AddPMActivity);
                break;

            default:
                Button b = (Button)v;
                String btntag = b.getTag().toString();
                String Content = b.getText().toString();
                Toast.makeText(this, "Please Wait Loading Data...", Toast.LENGTH_LONG).show();

                if(!PageType.equals("MachinePage")){

                    //myConfig.PmListTimer.cancel();
                    Intent clickM = new Intent(COMainPage.this,COMainPage.class);
                    clickM.putExtra("Type","MachinePage");
                    clickM.putExtra("LineId",btntag);
                    clickM.putExtra("LineName",Content);
                    myConfig.setCurrentPage("PmMachine");
                    startActivity(clickM);
                }
                else{
                    //myConfig.PmListTimer.cancel();
                    Intent clickM = new Intent(COMainPage.this, CoMachineDetail.class);
                    clickM.putExtra("PmId",btntag);
                    clickM.putExtra("MachineName",Content);
                    //startActivity(clickM);
                    myConfig.setCurrentPage("CoMachineDetail");
                    startActivityForResult(clickM, 100);

                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 100) {
            if(resultCode == Activity.RESULT_OK){
                GetPmMachine(LineId,true);
                myConfig.setCurrentPage("CoMachine");
                Log.d("TEST123","HERE");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                // myConfig.getCurrentPage().equals("PmLine");
            }
        }
    }

}
