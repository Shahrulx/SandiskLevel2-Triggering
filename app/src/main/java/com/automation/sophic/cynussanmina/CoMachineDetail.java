package com.automation.sophic.cynussanmina;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.automation.sophic.cynussanmina.Helper.LoadingClass;
import com.automation.sophic.cynussanmina.Model.CoModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CoMachineDetail extends AppCompatActivity implements View.OnClickListener {

    String PmId="",MachineName="";
    LinearLayout linearMain;
    TextView tvMachineName,tvLineName,tvDateCreated;
    TextView tvStartDate,tvTotalTime,tvTargetDuration;
    TextView tvPmType,tvTittle;
    Button btnBack,btnDone;
    RestCrud RestCrud;
    LoadingClass LoadActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_pmdetails);

        LoadActivity = new LoadingClass(this,1);
        PmId=getIntent().getStringExtra("PmId");
        MachineName=getIntent().getStringExtra("MachineName");

        linearMain = (LinearLayout) findViewById(R.id.linear_main);
        tvMachineName = (TextView) findViewById(R.id.tvMachineName);
        tvLineName = (TextView) findViewById(R.id.tvLineName);
        tvDateCreated = (TextView) findViewById(R.id.tvDateCreated);
        tvStartDate = (TextView) findViewById(R.id.tvStartDate);
        tvTotalTime = (TextView) findViewById(R.id.tvTotalTime);
        tvTargetDuration = (TextView) findViewById(R.id.tvTargetDuration);
        tvPmType = (TextView) findViewById(R.id.tvPmType);
        btnBack = (Button) findViewById(R.id.btnBack);
        btnDone = (Button) findViewById(R.id.btnDone);
        tvTittle =  (TextView) findViewById(R.id.tvTitle);
        tvTittle.setText("Change Over");

        tvMachineName.setText(MachineName);

        RestCrud = APIClient.getClient().create(RestCrud.class);

        GetCoMachineDetail(PmId);
        GetCoMachineChecklist(PmId);

        btnBack.setOnClickListener(this);
        btnDone.setOnClickListener(this);

    }

    public void GetCoMachineChecklist(final String PmId) {

        Call<List<CoModel.CoCheckList>> call = RestCrud.GetCoCheckList(PmId);
        call.enqueue(new Callback<List<CoModel.CoCheckList>>() {


            @Override
            public void onResponse(Call<List<CoModel.CoCheckList>> call, Response<List<CoModel.CoCheckList>> response) {

                List<CoModel.CoCheckList> body = response.body();
                if (body == null) {
                    return;
                }


                int numofdata = body.size();

                for(int i=0; i<numofdata; i++)
                {
                    final CheckBox checkBox = new CheckBox(CoMachineDetail.this);

                    checkBox.setId(body.get(i).Id);
                    checkBox.setText(body.get(i).Others);

                    if(body.get(i).PIC != null){
                        checkBox.setChecked(true);
                    }

                    checkBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int cboxid = checkBox.getId();

                            if (checkBox.isChecked())
                            {
                                String checked = checkBox.getText().toString();

                                UpdateCheckList(cboxid,"True");
                                Toast.makeText(CoMachineDetail.this,  " Checked",Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                String unchecked = checkBox.getText().toString();
                                UpdateCheckList(cboxid,"False");
                                Toast.makeText(CoMachineDetail.this,  " Unchecked",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    linearMain.addView(checkBox);

                    // Set Flex CheckBox scaling programmatically
                    checkBox.setScaleX(1.5f);
                    checkBox.setScaleY(1.5f);
                    // Get the margins of Flex CheckBox
                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) checkBox.getLayoutParams();

                    // Set left, top, right and bottom margins of Flex CheckBox
                    mlp.setMargins(50,20,50,20);
                    checkBox.setLayoutParams(mlp);

                    // Apply right padding of Flex CheckBox
                    //checkBox.setPadding(0,0,0,0);
                }


            }


            @Override
            public void onFailure(Call<List<CoModel.CoCheckList>> call, Throwable t) {
                Log.d("err","error get");
            }
        });

    }

    private void UpdateCheckList(int CheckListId,String Status)
    {
        String body = CheckListId+"|"+MainActivity.username+"|"+Status;
        Call<String> call = null;
        call = RestCrud.UpdateCoCheckList(body);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.d("UpdateCheckList","Success");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("UpdateCheckList","ERROR");
                Toast.makeText(CoMachineDetail.this, "Error Updating Data !", Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });

    }

    public void GetCoMachineDetail(String PmId) {

        Call<List<CoModel.GetCoMachineDetail>> call = RestCrud.GetCoMachineDetail(PmId);
        call.enqueue(new Callback<List<CoModel.GetCoMachineDetail>>() {


            @Override
            public void onResponse(Call<List<CoModel.GetCoMachineDetail>> call, Response<List<CoModel.GetCoMachineDetail>> response) {

                List<CoModel.GetCoMachineDetail> body = response.body();
                // if (body == null) {
                //    return;
                // }


                tvMachineName.setText(body.get(0).StationNameMap);
                tvLineName.setText("Line:"+body.get(0).LineNameMap);

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String dateInString = body.get(0).StartDate;
                dateInString=dateInString.replace("T"," ");
                try {
                    dateInString = outFormat.format(formatter.parse(dateInString));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tvStartDate.setText("Start Date:"+dateInString);


                Date currentDate = new Date();
                try {
                    Date StartDate = formatter.parse(dateInString);
                    long diff = currentDate.getTime() - StartDate.getTime();
                    long seconds = diff / 1000;
                    long minutes = seconds / 60;
                    tvTotalTime.setText("TotalTime:"+String.valueOf(minutes));

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                dateInString = body.get(0).DateTime;
                dateInString=dateInString.replace("T"," ");
                try {
                    dateInString = outFormat.format(formatter.parse(dateInString));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tvDateCreated.setText("Date Created:"+dateInString);

                tvTargetDuration.setText("TargetDuration:"+String.valueOf(body.get(0).Duration));
                tvPmType.setText("Type:"+body.get(0).COType);


            }

            @Override
            public void onFailure(Call<List<CoModel.GetCoMachineDetail>> call, Throwable t) {
                Log.d("err","error get GetCoMachineDetail");
            }
        });

    }

    private void DonePM(){

        String body = PmId+","+MainActivity.username;
        Call<String> call = null;

        LoadActivity.show();
        call = RestCrud.CloseCo(body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                LoadActivity.dismiss();
                String result = response.body();
                result = result.replace("\"", "");

                if(result.equals("true")) {
                    Toast.makeText(CoMachineDetail.this, "CO Closed!", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent();
                    //intent.putExtra("pos", 1);
                    setResult(Activity.RESULT_OK, intent);

                    finish();
                }
                else if(result.contains("false")) {
                    Toast.makeText(CoMachineDetail.this, "Please Close Checklist", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                LoadActivity.dismiss();
                Toast.makeText(CoMachineDetail.this, "Error Updating Data !", Toast.LENGTH_SHORT).show();
                call.cancel();

                Intent intent = new Intent();
                //intent.putExtra("pos", 1);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

    }

    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnBack:
                Intent intent = new Intent();
                //intent.putExtra("pos", 1);
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
                break;

            case R.id.btnDone:
                Toast.makeText(CoMachineDetail.this,"Please Wait Updating Data",Toast.LENGTH_LONG);
                DonePM();
                break;

            default:

                break;
        }
    }

}
